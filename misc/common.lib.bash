
# shellcheck disable=SC2034
xsession_default='/etc/X11/Xsession.x11-orig'
# shellcheck disable=SC2034
xsession_override='/etc/X11/Xsession.override'

# shellcheck disable=SC2034
systemd_user_directory="${XDG_RUNTIME_DIR}/systemd/user"
window_manager_unit_conf="${systemd_user_directory}/window-manager.target.d/window-manager.target.conf"

x11_usersession_directory="${XDG_RUNTIME_DIR}/x11-usersession"
# shellcheck disable=SC2034
setup_x11_usersession="${x11_usersession_directory}/setup.bash"
# shellcheck disable=SC2034
start_x11_usersession="${x11_usersession_directory}/start.bash"
# shellcheck disable=SC2034
stop_x11_usersession="${x11_usersession_directory}/stop.bash"
# shellcheck disable=SC2034
action_after_x11_usersession="${x11_usersession_directory}/action-after.bash"

# shellcheck disable=SC2034
x11_variables=(DISPLAY XAUTHORITY XDG_RUNTIME_DIR XDG_GREETER_DATA_DIR XDG_SEAT
  XDG_SEAT_PATH XDG_VTNR XDG_SESSION_ID XDG_SESSION_PATH
  XDG_SESSION_TYPE XDG_CURRENT_DESKTOP XDG_SESSION_DESKTOP DESKTOP_SESSION
  XDG_DATA_HOME XDG_STATE_HOME XDG_CACHE_HOME XDG_CONFIG_HOME RICH_DATA_HOME)
