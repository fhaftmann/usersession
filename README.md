# usersession

X11 session process managed as systemd user service.

The traditional X11 startup behaviour is modified to start the X11 session process as systemd user service.

Beware: many arcane traditional configuration slots of X11 are discarded by installing this package!
