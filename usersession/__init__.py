
"""Python library for user sessions managed by systemd / logind."""

# Author: 2024 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License along
# with this program. If not, see <https://www.gnu.org/licenses/>.


__all__ = ['long_description', 'error', 'check_root', 'env_for', 'Login_Bus']


from typing import NoReturn, Optional, Any
from collections.abc import Iterator, Mapping
import os
import sys
import pydbus  # type: ignore


long_description = """
    Fundamental interfaces for querying and managing systemd / logind user
    sessions.
"""


def error(msg: str) -> NoReturn:

    print(msg, file = sys.stderr)
    raise SystemExit(1)


def check_root() -> None:

    if os.getuid() != 0:
        error('Must be run as root.')


def env_for(uid: int) -> Mapping[str, str]:

    return {**os.environ, **dict(
      DBUS_SESSION_BUS_ADDRESS = f'unix:path=/run/user/{uid}/bus',
      XDG_RUNTIME_DIR = f'/run/user/{uid}')}


class Login_Bus:

    def get_system_bus(self) -> Any:

        return pydbus.SystemBus()

    def get_logind(self) -> Any:

        return self.get_system_bus().get('.login1')

    def get_data(self, object_path: str) -> Any:

        return self.get_system_bus().get('org.freedesktop.login1', object_path)

    def get_all_sessions(self) -> Iterator[tuple[str, int, str, str, Any]]:

        for session, uid, user, seat, object_path in self.get_logind().ListSessions():
            data = self.get_data(object_path)
            yield session, uid, user, seat, data

    def get_active_session(self) -> Optional[tuple[str, int, str, str, Any]]:

        for session, uid, user, seat, data in self.get_all_sessions():
            if data.Active:
                return session, uid, user, seat, data
        return None

    def get_all_graphical_sessions(self) -> Iterator[tuple[str, int, str, str, Any]]:

        for session, uid, user, seat, data in self.get_all_sessions():
            if data.Type not in ['x11', 'mir', 'wayland']:
                continue
            yield session, uid, user, seat, data

    def get_graphical_session_by_uid(self, uid: int) -> Optional[tuple[str, str, str, Any]]:

        for session, session_uid, user, seat, data in self.get_all_graphical_sessions():
            if session_uid == uid:
                return session, user, seat, data
        return None

    def get_graphical_session_by_name(self, user: str) -> Optional[tuple[str, int, str, Any]]:

        for session, uid, session_user, seat, data in self.get_all_graphical_sessions():
            if session_user == user:
                return session, uid, seat, data
        return None

    def get_all_users(self) -> Iterator[tuple[int, str, Any]]:

        for uid, user, object_path in self.get_logind().ListUsers():
            data = self.get_data(object_path)
            yield uid, user, data

    def get_user_by_uid(self, uid: int) -> Optional[tuple[str, Any]]:

        for session_uid, user, data in self.get_all_users():
            if session_uid == uid:
                return user, data
        return None

    def get_user_by_name(self, user: str) -> Optional[tuple[int, Any]]:

        for uid, session_user, data in self.get_all_users():
            if session_user == user:
                return uid, data
        return None

    def activate_session(self, session: str) -> None:

        self.get_logind().ActivateSession(session)

    def unlock_session(self, session: str) -> None:

        self.get_logind().UnlockSession(session)

    def teminate_session(self, session: str) -> None:

        self.get_logind().TerminateSession(session)

    def switch_to_greeter(self) -> None:

        for session, uid, session_user, seat, data in self.get_all_graphical_sessions():
            if data.Class == 'greeter':
                self.activate_session(session)
                return

        seat_path = os.getenv('XDG_SEAT_PATH')
        self.get_system_bus().get('org.freedesktop.DisplayManager', seat_path).SwitchToGreeter()
